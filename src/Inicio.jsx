import React, {useContext} from "react";
import TraductorContext from "./TraductorContext.js";


function Inicio() {
  const Traductor = useContext(TraductorContext);
  return (
    <div className="text-center">
      <br />
      <br />
      <i className="fa fa-4x fa-users"></i>
      <br />
      <h2>{Traductor.traduce("titol")}</h2>
    </div>
  );
}

export default Inicio;
