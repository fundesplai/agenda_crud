import React, { useState } from "react";

import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Button } from 'reactstrap';

// importamos los componentes de la aplicación (vistas)
import Inicio from './Inicio';
import Lista from './Lista';
import NuevoContacto from './NuevoContacto';
import ModificaContacto from './ModificaContacto';
import EliminaContacto from './EliminaContacto';
import P404 from './P404';

// importamos css
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './App.css';

// contexto donde guardaremos la función traduce y el idioma actual
import TraductorContext from "./TraductorContext";

// datos y contexto para traducciones
import diccionario from "./diccionario";


//m8sWZ2nMNstKVcAF6eJX 


// const contactosInicio = [
//   {
//     id: 1,
//     nombre: "indiana",
//     email: "indiana@jones.com"
//   },
//   {
//     id: 2,
//     nombre: "007",
//     email: "bond@james.bond"
//   },
//   {
//     id: 3,
//     nombre: "spiderman",
//     email: "peter@parker.com"
//   }
// ];


// clase App 
const App = () => {

  const [contactos, setContactos] = useState(loadData());
  const [idioma, setIdioma] = useState(0);

  // función traduce, devuelve el string correspondiente a la etiqueta e idioma facilitados
  const traduce = (etiqueta) => diccionario[etiqueta][idioma];


  //extra guardado de datos
  function saveData(contactosAGuardar) {
    var jsonData = JSON.stringify(contactosAGuardar);
    localStorage.setItem("datagenda", jsonData);
  }

  //carga de datos
  function loadData() {
    var text = localStorage.getItem("datagenda");
    if (text) {
      var obj = JSON.parse(text);
      return obj;
    }
    return [];
  }


  function guardaContacto(contacto) {
    //eliminamos version actual del contacto
    let nuevaLista = contactos.filter(el => el.id !== contacto.id);
    // añadimos la nueva version
    nuevaLista.push(contacto);
    //asignamos a contactos
    setContactos(nuevaLista);
    saveData(nuevaLista);
  }

  function nuevoContacto(contacto) {
    // s miramos el id del ultimo contacto y sumamos 1 
    contacto.id = contactos[contactos.length - 1].id + 1;
    const nuevaLista = [...contactos, contacto];
    //asignamos a contactos
    setContactos(nuevaLista);
    saveData(nuevaLista);
  }


  function eliminaContacto(idEliminar) {
    //creamos lista a partir de contactos, sin el contacto con el id recibido
    const nuevaLista = contactos.filter(el => el.id !== idEliminar);
    //asignamos a contactos
    setContactos(nuevaLista);
    saveData(nuevaLista);
  }



  return (
    <TraductorContext.Provider value={{ traduce, idioma }}>
      <BrowserRouter>
      <br />
      <br />
        <Container>
          <Row>
            <Col>
              <ul className="list-unstyled menu">
                <li> <Link className="link" to="/">Inicio</Link> </li>
                <li> <Link className="link" to="/lista">Contactos</Link> </li>
              </ul>
            </Col>
            <Col className="text-right">
              <Button
                size="sm"
                color="primary"
                outline={idioma === 0 ? false : true}
                onClick={() => setIdioma(0)}
              >
                ES
              </Button>{" "}
              <Button
                size="sm"
                color="primary"
                outline={idioma === 1 ? false : true}
                onClick={() => setIdioma(1)}
              >
                CA
              </Button>
            </Col>
          </Row>
          <Row>
            <Col>
              <Switch>
                <Route exact path="/" component={Inicio} />
                <Route path="/lista" render={() => <Lista contactos={contactos} />} />
                <Route path="/nuevo" render={() => <NuevoContacto nuevoContacto={nuevoContacto} />} />
                <Route path="/modifica/:id" render={(props) => <ModificaContacto id={props.match.params.id} contactos={contactos} guardaContacto={guardaContacto} />} />
                <Route path="/elimina/:id" render={(props) => <EliminaContacto id={props.match.params.id} contactos={contactos} eliminaContacto={eliminaContacto} />} />
                <Route component={P404} />
              </Switch>
            </Col>
          </Row>

          {/* <Row>
          <Col>
          <br />
          <br />
          <br />
            <Button className="btn-primary btn-sm" onClick={loadData}>Cargar datos</Button>
            {' '}
            <Button className="btn-success btn-sm" onClick={saveData}>Guardar datos</Button>
          </Col>
        </Row> */}

        </Container>
      </BrowserRouter>
    </TraductorContext.Provider>

  );

}

export default App;